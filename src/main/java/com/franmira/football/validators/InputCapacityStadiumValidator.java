package com.franmira.football.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.franmira.football.dto.InputTeamDto;

public class InputCapacityStadiumValidator implements ConstraintValidator<InputCapacityStadium, InputTeamDto> {

	@Override
	public boolean isValid(InputTeamDto value, ConstraintValidatorContext context) {
		return value.getDivision() == 1 && value.getStadiumCapacity() > 50000
				|| value.getDivision() == 2 && value.getStadiumCapacity() > 10000
				|| value.getDivision() == 3 && value.getStadiumCapacity() > 3000;
	}
}

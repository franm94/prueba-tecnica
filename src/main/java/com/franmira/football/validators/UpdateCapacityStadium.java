package com.franmira.football.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UpdateCapacityStadiumValidator.class)
@Documented
public @interface UpdateCapacityStadium {
	
	  String message() default "The capacity of stadium must be greater";

	  Class<?>[] groups() default { };

	  Class<? extends Payload>[] payload() default { };
}

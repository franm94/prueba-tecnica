package com.franmira.football.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.franmira.football.entities.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {
	List<Team> findByOrderByStadiumCapacityAsc();
}

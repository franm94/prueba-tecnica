package com.franmira.football.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Team implements Serializable {

	@GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "sequence", allocationSize = 1)
	@Id
	private Long id;
	private String name;
	private String city;
	private String owner;
	private Integer stadiumCapacity;
	private Integer division;
	private String competition;
	private Integer totalPlayers;
	private LocalDate createDate;

}

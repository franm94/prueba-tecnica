package com.franmira.football.dto;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputTeamDto implements Serializable {
	private Long id;
	private String name;
	private String city;
	private String owner;
	private Integer stadiumCapacity;
	private Integer division;
	private String competition;
	private Integer totalPlayers;
	private LocalDate createDate;
}

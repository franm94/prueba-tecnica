package com.franmira.football.dto;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.franmira.football.validators.UpdateCapacityStadium;

import lombok.Getter;

@Getter
@UpdateCapacityStadium(message = "The capacity of stadium must be greater")
public class UpdateTeamDto implements Serializable {
	private Long id;
	@NotEmpty(message = "Please provide a name")
	private String name;
	private String city;
	private String owner;
	@Min(value = 1, message = "The value must be positive")
	private Integer stadiumCapacity;
	@Min(value = 1, message = "The value must be between 1-3")
	@Max(value = 3, message = "The value must be between 1-3")
	private Integer division;
	private String competition;
	@Min(value = 1, message = "The value must be positive")
	private Integer totalPlayers;
}

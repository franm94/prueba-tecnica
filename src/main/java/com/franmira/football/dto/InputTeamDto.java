package com.franmira.football.dto;

import java.time.LocalDate;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;

import com.franmira.football.validators.InputCapacityStadium;

import lombok.Getter;

@Getter
@InputCapacityStadium(message = "The capacity of stadium must be greater")
public class InputTeamDto {
	@NotEmpty(message = "Please provide a name")
	private String name;
	private String city;
	private String owner;
	@Min(value = 0, message = "The value must be positive")
	private Integer stadiumCapacity;
	@Min(value = 1, message = "The value must be between 1-3")
	@Max(value = 3, message = "The value must be between 1-3")
	private Integer division;
	private String competition;
	@Min(value = 1, message = "The value must be positive")
	private Integer totalPlayers;
	@Past(message = "The date must be older than today date")
	private LocalDate createDate;
}

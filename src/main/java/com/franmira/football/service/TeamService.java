package com.franmira.football.service;

import java.util.List;

import com.franmira.football.dto.InputTeamDto;
import com.franmira.football.dto.OutputTeamDto;
import com.franmira.football.dto.UpdateTeamDto;

public interface TeamService {

	Long save(InputTeamDto team);

	List<OutputTeamDto> findAll();
	
	List<OutputTeamDto> findByOrderByStadiumCapacityAsc();
	
	OutputTeamDto findById(Long id);
	
	void update(UpdateTeamDto updateTeamDto);
	
	void delete(Long id);
}

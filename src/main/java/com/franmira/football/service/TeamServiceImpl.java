package com.franmira.football.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.franmira.football.dto.InputTeamDto;
import com.franmira.football.dto.OutputTeamDto;
import com.franmira.football.dto.UpdateTeamDto;
import com.franmira.football.entities.Team;
import com.franmira.football.exceptions.ResourceNotFoundException;
import com.franmira.football.mapper.TeamMapper;
import com.franmira.football.repository.TeamRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class TeamServiceImpl implements TeamService {

	private final TeamRepository teamRepository;
	private final TeamMapper teamMapper;

	@Override
	public Long save(InputTeamDto inputTeamDto) {
		Team team = teamMapper.dtoToEntity(inputTeamDto);
		return teamMapper.entityToDto(teamRepository.save(team)).getId();
	}

	@Override
	public List<OutputTeamDto> findAll() {
		return teamMapper.entitiesToDtos(teamRepository.findAll());
	}
	
	@Override
	public List<OutputTeamDto> findByOrderByStadiumCapacityAsc() {
		return teamMapper.entitiesToDtos(teamRepository.findByOrderByStadiumCapacityAsc());
	}

	@Override
	public OutputTeamDto findById(Long id) {
		return teamMapper.entityToDto(teamRepository.findById(id).orElse(null));
	}

	@Override
	public void update(UpdateTeamDto updateTeamDto) {
		Team team = teamRepository.findById(updateTeamDto.getId()).orElseThrow(
				() -> new ResourceNotFoundException("Team not found for this id :: " + updateTeamDto.getId()));

		team.setCity(updateTeamDto.getCity());
		team.setCompetition(updateTeamDto.getCompetition());
		team.setDivision(updateTeamDto.getDivision());
		team.setName(updateTeamDto.getName());
		team.setOwner(updateTeamDto.getOwner());
		team.setStadiumCapacity(updateTeamDto.getStadiumCapacity());
		team.setTotalPlayers(updateTeamDto.getTotalPlayers());
		teamRepository.save(team);
	}

	@Override
	public void delete(Long id) {
		teamRepository.deleteById(id);
	}

}

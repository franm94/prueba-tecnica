package com.franmira.football.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.franmira.football.dto.InputTeamDto;
import com.franmira.football.dto.OutputTeamDto;
import com.franmira.football.dto.UpdateTeamDto;
import com.franmira.football.entities.Team;

@Mapper
public interface TeamMapper {
	
	OutputTeamDto entityToDto(Team entity);

	Team dtoToEntity(InputTeamDto dto);
	
	Team dtoToEntity(OutputTeamDto dto);
	
	Team dtoToEntity(UpdateTeamDto dto);
	
	List<OutputTeamDto> entitiesToDtos(List<Team> entities);

	List<Team> DtosToEntities(List<OutputTeamDto> dtos);
}

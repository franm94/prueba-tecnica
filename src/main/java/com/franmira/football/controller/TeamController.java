package com.franmira.football.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.franmira.football.dto.InputTeamDto;
import com.franmira.football.dto.OutputTeamDto;
import com.franmira.football.dto.UpdateTeamDto;
import com.franmira.football.service.TeamService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping(value = "/team")
@SecurityRequirement(name = "franmira")
@Slf4j
public class TeamController {

	private final TeamService teamService;

	@PostMapping
	@PreAuthorize ("hasRole('ROLE_ADMIN')")
	@Operation(description = "Create a team")
	public ResponseEntity<Long> create(@Valid @RequestBody final InputTeamDto team) {
		log.info("Creating " + team.getName() + "");
		return new ResponseEntity<>(teamService.save(team), HttpStatus.CREATED);
	}
	
	@GetMapping
	@Operation(description = "Get all teams")
	public ResponseEntity<List<OutputTeamDto>> getAll() {
		log.info("Getting all teams ordered by capacity...");
		return new ResponseEntity<>(teamService.findByOrderByStadiumCapacityAsc(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	@Operation(description = "Get a specific team")
	public ResponseEntity<OutputTeamDto> getById(@RequestParam(required = true) Long id) {
		log.info("Getting a team with id: " + id);
		return new ResponseEntity<>(teamService.findById(id), HttpStatus.OK);
	}

	@PutMapping
	@Operation(description = "Update a team")
	public ResponseEntity<HttpStatus> update(@Valid @RequestBody final UpdateTeamDto updateTeamDto) {
		log.info("Triying to update " + updateTeamDto.getName() + " team");
		teamService.update(updateTeamDto);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@Operation(description = "Delete a team")
	public ResponseEntity<HttpStatus> delete(@RequestParam(required = true) Long id) {
		log.info("Triying to delete a team with id: " + id);
		teamService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}

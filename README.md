# Football-app

## Getting started

### Docker run in local
docker run -p8080:8080 franmira/football-app

### Deploy in kubernetes
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml

